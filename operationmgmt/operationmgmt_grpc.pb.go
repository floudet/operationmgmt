// Code generated by protoc-gen-go-grpc. DO NOT EDIT.

package operationmgmt

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion7

// OperationManagementClient is the client API for OperationManagement service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type OperationManagementClient interface {
	CreateNewOperation(ctx context.Context, in *NewOperation, opts ...grpc.CallOption) (*Operation, error)
	GetOperations(ctx context.Context, in *GetOperationDetails, opts ...grpc.CallOption) (*OperationList, error)
	GetOperationCounter(ctx context.Context, in *CounterParams, opts ...grpc.CallOption) (*Counter, error)
	DeleteOperation(ctx context.Context, in *OperationId, opts ...grpc.CallOption) (*Operation, error)
}

type operationManagementClient struct {
	cc grpc.ClientConnInterface
}

func NewOperationManagementClient(cc grpc.ClientConnInterface) OperationManagementClient {
	return &operationManagementClient{cc}
}

func (c *operationManagementClient) CreateNewOperation(ctx context.Context, in *NewOperation, opts ...grpc.CallOption) (*Operation, error) {
	out := new(Operation)
	err := c.cc.Invoke(ctx, "/operationmgmt.OperationManagement/CreateNewOperation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *operationManagementClient) GetOperations(ctx context.Context, in *GetOperationDetails, opts ...grpc.CallOption) (*OperationList, error) {
	out := new(OperationList)
	err := c.cc.Invoke(ctx, "/operationmgmt.OperationManagement/GetOperations", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *operationManagementClient) GetOperationCounter(ctx context.Context, in *CounterParams, opts ...grpc.CallOption) (*Counter, error) {
	out := new(Counter)
	err := c.cc.Invoke(ctx, "/operationmgmt.OperationManagement/GetOperationCounter", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *operationManagementClient) DeleteOperation(ctx context.Context, in *OperationId, opts ...grpc.CallOption) (*Operation, error) {
	out := new(Operation)
	err := c.cc.Invoke(ctx, "/operationmgmt.OperationManagement/DeleteOperation", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// OperationManagementServer is the server API for OperationManagement service.
// All implementations must embed UnimplementedOperationManagementServer
// for forward compatibility
type OperationManagementServer interface {
	CreateNewOperation(context.Context, *NewOperation) (*Operation, error)
	GetOperations(context.Context, *GetOperationDetails) (*OperationList, error)
	GetOperationCounter(context.Context, *CounterParams) (*Counter, error)
	DeleteOperation(context.Context, *OperationId) (*Operation, error)
	mustEmbedUnimplementedOperationManagementServer()
}

// UnimplementedOperationManagementServer must be embedded to have forward compatible implementations.
type UnimplementedOperationManagementServer struct {
}

func (UnimplementedOperationManagementServer) CreateNewOperation(context.Context, *NewOperation) (*Operation, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateNewOperation not implemented")
}
func (UnimplementedOperationManagementServer) GetOperations(context.Context, *GetOperationDetails) (*OperationList, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOperations not implemented")
}
func (UnimplementedOperationManagementServer) GetOperationCounter(context.Context, *CounterParams) (*Counter, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetOperationCounter not implemented")
}
func (UnimplementedOperationManagementServer) DeleteOperation(context.Context, *OperationId) (*Operation, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteOperation not implemented")
}
func (UnimplementedOperationManagementServer) mustEmbedUnimplementedOperationManagementServer() {}

// UnsafeOperationManagementServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to OperationManagementServer will
// result in compilation errors.
type UnsafeOperationManagementServer interface {
	mustEmbedUnimplementedOperationManagementServer()
}

func RegisterOperationManagementServer(s grpc.ServiceRegistrar, srv OperationManagementServer) {
	s.RegisterService(&_OperationManagement_serviceDesc, srv)
}

func _OperationManagement_CreateNewOperation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(NewOperation)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OperationManagementServer).CreateNewOperation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/operationmgmt.OperationManagement/CreateNewOperation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OperationManagementServer).CreateNewOperation(ctx, req.(*NewOperation))
	}
	return interceptor(ctx, in, info, handler)
}

func _OperationManagement_GetOperations_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetOperationDetails)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OperationManagementServer).GetOperations(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/operationmgmt.OperationManagement/GetOperations",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OperationManagementServer).GetOperations(ctx, req.(*GetOperationDetails))
	}
	return interceptor(ctx, in, info, handler)
}

func _OperationManagement_GetOperationCounter_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CounterParams)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OperationManagementServer).GetOperationCounter(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/operationmgmt.OperationManagement/GetOperationCounter",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OperationManagementServer).GetOperationCounter(ctx, req.(*CounterParams))
	}
	return interceptor(ctx, in, info, handler)
}

func _OperationManagement_DeleteOperation_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(OperationId)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(OperationManagementServer).DeleteOperation(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/operationmgmt.OperationManagement/DeleteOperation",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(OperationManagementServer).DeleteOperation(ctx, req.(*OperationId))
	}
	return interceptor(ctx, in, info, handler)
}

var _OperationManagement_serviceDesc = grpc.ServiceDesc{
	ServiceName: "operationmgmt.OperationManagement",
	HandlerType: (*OperationManagementServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateNewOperation",
			Handler:    _OperationManagement_CreateNewOperation_Handler,
		},
		{
			MethodName: "GetOperations",
			Handler:    _OperationManagement_GetOperations_Handler,
		},
		{
			MethodName: "GetOperationCounter",
			Handler:    _OperationManagement_GetOperationCounter_Handler,
		},
		{
			MethodName: "DeleteOperation",
			Handler:    _OperationManagement_DeleteOperation_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "operationmgmt.proto",
}
