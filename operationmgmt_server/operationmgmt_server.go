package main

import(
	"context"
	"log"
	"net"
	"fmt"

	pb "gitlab.com/floudet/operationmgmt/operationmgmt"
	"google.golang.org/grpc"
)

const (
	port = ":5555"
)

var operation_counter int32 = 31 

func NewOperationManagementServer() *OperationManagementServer{
	return &OperationManagementServer{
		operation_list: &pb.OperationList{},
	}
}

func (server *OperationManagementServer) Run() error{
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	s := grpc.NewServer()
	pb.RegisterOperationManagementServer(s, server)
	log.Printf("server listening at %v", lis.Addr())
	return s.Serve(lis)
}

type OperationManagementServer struct {
	pb.UnimplementedOperationManagementServer
	operation_list *pb.OperationList
}

func (s *OperationManagementServer) CreateNewOperation(ctx context.Context, in *pb.NewOperation) (*pb.Operation, error){
	log.Printf("Received: %v", in.GetName())
	operation_counter+=1
	var operation_id int32 = operation_counter
	created_operation := &pb.Operation{Name: fmt.Sprintf("rt0%d - %s",operation_counter, in.GetName()), Id: operation_id}	
	s.operation_list.Operations = append(s.operation_list.Operations, created_operation)
	return created_operation, nil
}

func (s *OperationManagementServer) GetOperations(ctx context.Context, in *pb.GetOperationDetails) (*pb.OperationList, error){
	return s.operation_list, nil
}

func (s *OperationManagementServer) GetOperationCounter(ctx context.Context, in *pb.CounterParams) (*pb.Counter, error){
	return &pb.Counter{Counter: operation_counter}, nil
}

func (s *OperationManagementServer) DeleteOperation(ctx context.Context, in *pb.OperationId) (*pb.Operation, error){
	index:=search_operation(s.operation_list.Operations, in.GetId())
	if index == 99999{
		return nil,nil
	}
	operation_to_delete := s.operation_list.Operations[index]
	s.operation_list.Operations = remove(s.operation_list.Operations, index)
	return operation_to_delete, nil
}

func search_operation(op_list []*pb.Operation, itemToSearch int32) (int32){
	var found int32 = 99999
	fmt.Println(itemToSearch)
	for index, v := range op_list {
		if v.Id == itemToSearch {
			found = int32(index)	
		}
	}
	return found
}

func remove(op_list []*pb.Operation, itemToDelete int32) []*pb.Operation {
	return append(op_list[:itemToDelete], op_list[itemToDelete+1:]...)
	// TODO: check that it exists first or if not return nothing
}

func main(){
	var operation_mgmt_server *OperationManagementServer = NewOperationManagementServer()
	if err := operation_mgmt_server.Run(); err !=nil {
		log.Fatalf("failed to serve: %v", err)
	}
}