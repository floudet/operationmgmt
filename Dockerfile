# syntax=docker/dockerfile:1

FROM golang:1.15-alpine

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN go mod download

RUN apk add git
RUN go get -u gitlab.com/floudet/operationmgmt/operationmgmt

COPY ./operationmgmt ./operationmgmt
COPY ./operationmgmt_server ./operationmgmt_server

RUN ls -lah ./

EXPOSE 5555
#RUN go run ./operationmgmt_server/operationmgmt_server.go
RUN go build -o operationmgmt_api ./operationmgmt_server/operationmgmt_server.go
EXPOSE 5555
CMD [ "./operationmgmt_api" ]
