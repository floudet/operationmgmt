package main

import(
	"context"
	"log"
	"time"

	pb "gitlab.com/floudet/operationmgmt/operationmgmt"
	"google.golang.org/grpc"
)

const (
	address = "localhost:5555"
)

func main() {

	conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := pb.NewOperationManagementClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()

	r, err := c.CreateNewOperation(ctx, &pb.NewOperation{Name: "rt004"})
	if err != nil {
		log.Fatalf("could not create Operation: %v", err)
	}
	log.Printf(`Operation Details:
NAME: %s
ID: %d`, r.GetName(), r.GetId())

	details := &pb.GetOperationDetails{}
	resp, err := c.GetOperations(ctx, details)
	if err !=nil {
		log.Fatalf("could not retrieve operations: %v", err)
	}
	log.Printf("\nOperation List: \n")
	log.Printf("r.GetOperations(): %v\n", resp.GetOperations())
}