module gitlab.com/floudet/operationmgmt

go 1.15

require (
	github.com/golang/protobuf v1.5.2
	gitlab.com/floudet/operationmgmt/operationmgmt v0.0.0-20211126144316-4240b2a00ab8 // indirect
	google.golang.org/grpc v1.42.0
	google.golang.org/protobuf v1.27.1
)
